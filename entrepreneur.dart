import 'package:flutter/material.dart';

class scholar extends StatefulWidget {
  const scholar({Key? key}) : super(key: key);

  @override
  State<scholar> createState() => scholarState();
}

class scholarState extends State<scholar> {
  List<String> name = [
    'small business'
        'Large Company'
        'Scalable StartUP'
        'International'
        "Socail"
        "Enviromental"
        'Tech'
        'Innovation'
  ];
  List<String> tempArray = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Select fields of intrest"),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: ListView.builder(
                  itemCount: name.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        setState(() {
                          if (tempArray.contains(name[index].toString())) {
                            tempArray.remove(name[index].toString());
                          } else {
                            tempArray.add(name[index].toString());
                          }
                        });
                        print("Your entrepreneurship field of interest are:");
                        print(tempArray.toString());
                      },
                      child: ListTile(
                        title: Text(name[index].toString()),
                        trailing: Container(
                          height: 50,
                          width: 120,
                          decoration: BoxDecoration(
                            color: tempArray.contains(name[index].toString())
                                ? Colors.red
                                : Colors.blue,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Center(
                            child: Text(
                                tempArray.contains(name[index].toString())
                                    ? "Remove"
                                    : "Add"),
                          ),
                        ),
                      ),
                    );
                  }))
        ],
      )),
    );
  }
}
