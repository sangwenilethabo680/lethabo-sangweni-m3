import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My first App",
      home: Scaffold(
        appBar: AppBar(title: const Text(" welcome my firstapp")),
        body: Center(child: Text("Lethabo Sangweni")),
      ),
    );
  }
}
